package org.socked.gwt.client.model.callbacks;

public interface IMessageCallback {

	void onMessage(String message);

}