package org.socked.gwt.client.model;

import com.google.gwt.core.client.JavaScriptObject;

public class Connection extends JavaScriptObject {

	protected Connection() {
	}

	public final native String getId() /*-{
		return this.getId();
	}-*/;

	public final native Channel getChannel(String channelName) /*-{
		return this.getChannel(channelName);
	}-*/;

	public final native void disconnect() /*-{
		this.disconnect();
	}-*/;
}
