package org.socked.gwt.client.model.callbacks;

public interface IConnectCallback {

	void onConnect();

}