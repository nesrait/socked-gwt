package org.socked.gwt.client.model;

import com.google.gwt.core.client.JavaScriptObject;

public class Channel extends JavaScriptObject {

	protected Channel() {
	}

	public final native String getName() /*-{
		return this.getName();
	}-*/;

	public final ChannelRef subscribe(Options options) {
		return subscribe(options.getJSO());
	}

	private final native ChannelRef subscribe(JavaScriptObject options) /*-{
		return this.subscribe(options);
	}-*/;
}
