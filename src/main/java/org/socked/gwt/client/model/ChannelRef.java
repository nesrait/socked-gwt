package org.socked.gwt.client.model;

import org.socked.gwt.client.model.callbacks.IConnectCallback;
import org.socked.gwt.client.model.callbacks.IDisconnectCallback;
import org.socked.gwt.client.model.callbacks.IMessageCallback;

import com.google.gwt.core.client.JavaScriptObject;

public class ChannelRef extends JavaScriptObject {

	protected ChannelRef() {
	}

	public final native ChannelRef onConnect(IConnectCallback callback) /*-{
		return this
				.onConnect(function() {
					callback.@org.socked.gwt.client.model.callbacks.IConnectCallback::onConnect()();
				});
	}-*/;

	public final native ChannelRef onMessage(IMessageCallback callback) /*-{
		return this
				.onMessage(function(message) {
					callback.@org.socked.gwt.client.model.callbacks.IMessageCallback::onMessage(Ljava/lang/String;)(message);
				});
	}-*/;

	public final native ChannelRef onDisconnect(IDisconnectCallback callback) /*-{
		return this
				.onDisconnect(function() {
					callback.@org.socked.gwt.client.model.callbacks.IDisconnectCallback::onDisconnect()();
				});
	}-*/;

	public final native void send(String message) /*-{
		this.send(message);
	}-*/;

	public final native void unsubscribe() /*-{
		this.unsubscribe();
	}-*/;

	public final void reSubscribe(Options options) {
		reSubscribe(options.getJSO());
	};

	private final native void reSubscribe(JavaScriptObject options) /*-{
		this.reSubscribe(options);
	}-*/;
}
