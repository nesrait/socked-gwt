package org.socked.gwt.client.model.callbacks;

public interface IDisconnectCallback {

	void onDisconnect();

}
