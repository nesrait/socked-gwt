package org.socked.gwt.client.model;

import java.util.Set;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

public class Options {

	public enum Role {
		BOTH, SENDER, RECEIVER
	};

	private Role role = Role.SENDER;
	private Set<String> interests = null;

	public Options(Role role, Set<String> interests) {
		this.role = role;
		this.interests = interests;
	}

	private native static JavaScriptObject getJSO(String role,
			JsArrayString interests) /*-{
		var options = {
			role : role,
			interests : interests
		};
		return options;
	}-*/;

	public JavaScriptObject getJSO() {
		String role = this.role.toString().toLowerCase();
		JsArrayString jsInterests = null;
		if (interests != null) {
			jsInterests = (JsArrayString) JsArrayString.createArray();
			int i = 0;
			for (String interest : interests) {
				jsInterests.set(i, interest);
				i++;
			}
		}
		return getJSO(role, jsInterests);
	}
}
