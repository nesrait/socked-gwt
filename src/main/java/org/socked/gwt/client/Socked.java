package org.socked.gwt.client;

import org.socked.gwt.client.model.Channel;
import org.socked.gwt.client.model.ChannelRef;
import org.socked.gwt.client.model.Connection;
import org.socked.gwt.client.model.Options;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

public class Socked extends JavaScriptObject {

	protected Socked() {
	}

	public interface SockedReady {
		void onReady();
	}

	public final static void ready(final SockedReady callback) {
		// TODO: figure out why the SockJS and Socket object aren't being inject
		// into $wnd as expected

		// String sockjsUrl = GWT.getModuleBaseURL() + "sockjs-0.2.min.js";
		// FromUrl a = ScriptInjector.fromUrl(sockjsUrl);
		// a.setWindow(window());
		// a.inject();
		//
		// String sockedUrl = GWT.getModuleBaseURL() + "socked-0.1.min.js";
		// FromUrl b = ScriptInjector.fromUrl(sockedUrl);
		// b.setWindow(window());
		// b.inject();

		ready0(callback);
	}

	private final native static JavaScriptObject window() /*-{
		return $wnd;
	}-*/;

	private final native static void ready0(SockedReady callback) /*-{
		try {
			// For debug
			console.log("$wnd.Socked", $wnd.Socked);
			$wnd.Socked
					.ready(function() {
						callback.@org.socked.gwt.client.Socked.SockedReady::onReady()();
					});
		} catch (e) {
			console.log(e)
		}
	}-*/;

	public final static Connection createConnection(String id, String server,
			String appKey, String[] channelNames) {
		JsArrayString arr = (JsArrayString) JsArrayString.createArray();
		if (channelNames != null) {
			int i = 0;
			for (String channelName : channelNames) {
				arr.set(i, channelName);
				i++;
			}
		}
		return createConnection0(id, server, appKey, arr);
	};

	private final native static Connection createConnection0(String id,
			String server, String appKey, JsArrayString channelNames) /*-{
		var channels = [];
		for ( var i = 0; i < channelNames.length; i++) {
			channels.push({
				name : channelNames[i]
			});
		}
		var connection = $wnd.Socked.createConnection({
			id : id,
			server : server,
			appKey : appKey,
			channels : channels
		});
		return connection;
	}-*/;

	public final native static Channel getChannel(String connectionId,
			String channelName) /*-{
		return $wnd.Socked.getChannel(connectionId, channelName);
	}-*/;

	public final static ChannelRef subscribe(String connectionId,
			String channelName, Options options) {
		return subscribe(connectionId, channelName, options.getJSO());
	}

	private final native static ChannelRef subscribe(String connectionId,
			String channelName, JavaScriptObject options) /*-{
		return $wnd.Socked.subscribe(connectionId, channelName, options);
	}-*/;
}
